'use strict';

angular
    .module('aguagente-front', [
        'ngAnimate',
        'ngResource',
        'ngSanitize',
        'ui.router',
        'ui.bootstrap',
        'aguagente-front-controllers',
        'aguagente-front-directives'
    ])
    .config(['$stateProvider', '$urlRouterProvider', 

        function($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.otherwise("/index/main");

            $stateProvider
                .state('index', {
                    abstract: true,
                    url: "/index",
                    templateUrl: "views/common/content.html",
                })
                .state('index.main', {
                    url: "/main",
                    templateUrl: "views/main.html",
                    data: {
                        pageTitle: 'Example view'
                    }
                })
                .state('index.minor', {
                    url: "/minor",
                    templateUrl: "views/minor.html",
                    data: {
                        pageTitle: 'Example view'
                    }
                })
        }
    ])

.run(['$rootScope', '$state', function($rootScope, $state) {

    $rootScope.$state = $state;

}]);